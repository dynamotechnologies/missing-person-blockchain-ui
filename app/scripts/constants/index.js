import keyMirror from 'fbjs/lib/keyMirror';
import dataRegistryArtifact from "../../../contracts/DataRegistry2.json"
/**
 * @namespace Constants
 * @desc App constants
 */

/**
 * @constant {Object} ActionTypes
 * @memberof Constants
 */
export const ActionTypes = keyMirror({
  USER_LOGIN_REQUEST: undefined,
  USER_LOGIN_SUCCESS: undefined,
  USER_LOGIN_FAILURE: undefined,
  USER_LOGOUT_REQUEST: undefined,
  USER_LOGOUT_SUCCESS: undefined,
  USER_LOGOUT_FAILURE: undefined,
  GITHUB_GET_REPOS_REQUEST: undefined,
  GITHUB_GET_REPOS_SUCCESS: undefined,
  GITHUB_GET_REPOS_FAILURE: undefined,
  SHOW_ALERT: undefined,
  HIDE_ALERT: undefined,

  // elastic
  ES_SEARCH_REQUEST: undefined,
  ES_SEARCH_SUCCESS: undefined,
  ES_SEARCH_FAILURE: undefined,

  // hyperledger
  HYPERLEDGER_GET_DIRECTORY_REQUEST: undefined,
  HYPERLEDGER_GET_DIRECTORY_SUCCESS: undefined,
  HYPERLEDGER_GET_DIRECTORY_FAILURE: undefined,

  //web3
  WEB3_UNLOCK_ACCOUNT_REQUEST: undefined,
  WEB3_UNLOCK_ACCOUNT_SUCCESS: undefined,
  WEB3_UNLOCK_ACCOUNT_FAILURE: undefined,
  WEB3_SEND_TRANSACTION_REQUEST: undefined,
  WEB3_SEND_TRANSACTION_SUCCESS: undefined,
  WEB3_SEND_TRANSACTION_FAILURE: undefined,
  WEB3_SEND_TO_DATA_REGISTRY_REQUEST: undefined,
  WEB3_SEND_TO_DATA_REGISTRY_SUCCESS: undefined,
  WEB3_SEND_TO_DATA_REGISTRY_FAILURE: undefined,
  WEB3_GET_TOP_DATA_REGISTRY_REQUEST: undefined,
  WEB3_GET_TOP_DATA_REGISTRY_SUCCESS: undefined,
  WEB3_GET_TOP_DATA_REGISTRY_FAILURE: undefined,
  WEB3_GET_CONTRACT_DATA_REQUEST: undefined,
  WEB3_GET_CONTRACT_DATA_SUCCESS: undefined,
  WEB3_GET_CONTRACT_DATA_FAILURE: undefined,
  WEB3_BATCH_UPLOAD_PERSON_SUCCESS: undefined,
  WEB3_BATCH_UPLOAD_PERSON_FAILURE: undefined,
  // ipfs
  IPFS_UPLOAD_REQUEST: undefined,
  IPFS_UPLOAD_SUCCESS: undefined,
  IPFS_UPLOAD_FAILURE: undefined,

  DB_SELECT_ROW: undefined
});

/**
 * @constant {Object} XHR
 * @memberof Constants
 */
export const XHR = keyMirror({
  SUCCESS: undefined,
  FAIL: undefined,
});

export const COUNTRIES = {
  CHINA: {
    name: 'china',
    url: 'http://ec2-54-183-149-214.us-west-1.compute.amazonaws.com:8080/api/org.country.citizen.Person'
  },
  USA: {
    name: 'usa',
    url: 'http://ec2-54-183-149-214.us-west-1.compute.amazonaws.com:8080/api/org.country.citizen.CitizenAsset'
  },
  PHILIPPINES: {
    name: 'philippines',
    url: 'http://ec2-54-183-149-214.us-west-1.compute.amazonaws.com:8080/api/org.country.citizen.CitizenAsset'
  }
}

// for test purposes with ganache, uncomment when needed
// export const WEB_3 = {
//   provider: 'http://localhost:7545',
//   account: '0xc2EFA0597Dd299B8B7cc0960dcac15F8f2927D30', // replace with an account from ganache client
//   contract: '',
//   password: ''
// }

// comment out if the above is not commented out
export const WEB_3 = {
  provider: 'http://localhost:8544',
  account: '0x92a998e2d04497ad3a453aaf3da9b1e86e04bc67',
  contract: '',
  password: 'DynamoIDChain',
  gas: 8000000
}

export const S3_ENDPOINT = 'https://s3.amazonaws.com/dynamo-trusted-data-initiative';
export const DATA_REGISTRY = dataRegistryArtifact

export const organizations = {
  dhs: {
    id: 'dhs',
    name: 'Department of Homeland Security',
    logo: require(`assets/media/images/dhs.svg`),
    details: {
      org_type: 'Federal Agency',
      chain: {
        type: 'Hyperledger',
        url: 'http://ec2-52-53-222-121.us-west-1.compute.amazonaws.com:8080/api/org.agency.people.Person'
      }
    }
  },
  fcpd: {
    id: 'fcpd',
    name: 'Fairfax County Police Department',
    logo: require(`assets/media/images/fc.svg`),
    details: {
      org_type: 'Local Government',
      chain: {
        type: 'Hyperledger',
        url: 'http://ec2-52-53-181-38.us-west-1.compute.amazonaws.com:8080/api/org.agency.people.Person'
      }
    }

  },
  gfems: {
    id: 'gfems',
    name: 'Global Fund to End Modern Slavery',
    logo: require(`assets/media/images/gfems.png`),
    details: {
      org_type: 'Non-Government',
      chain: {
        type: 'Hyperledger',
        url: 'http://ec2-54-193-8-204.us-west-1.compute.amazonaws.com:8080/api/org.agency.people.Person'
      }
    }
  },
  pub: {
    id: 'pub',
    name: 'Public',
    logo: require(`assets/media/images/eth.png`),
    details: {
      org_type: 'Public',
      chain: {
        type: 'Ethereum',
        elastic: {
          url: 'http://search-dynamo-hackathon-luhe6v5vah7nd5nrswov76xwue.us-east-1.es.amazonaws.com/missing_person/_search'
        }
      }
    }
  }
}

export const CATEGORY_NAME_MAP = [
  'Fingerprint', 
  'Faceshot', 
  'MissingPerson', 
  'CriminalRecord', 
  'TrafficAlert', 
  'Uncategorized'
]

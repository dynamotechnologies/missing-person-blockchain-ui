/**
 * Created by patrickjang on 4/13/18.
 */
// @flow
/**
 * @module Actions/User
 * @desc User Actions
 */
import { ActionTypes } from 'constants/index';

/**
 * Get Repos
 *
 * @param {string} org
 * @returns {Object}
 */
export function getDirectory(org: Object): Object {
  return {
    type: ActionTypes.HYPERLEDGER_GET_DIRECTORY_REQUEST,
    payload: { endpoint: org.url },
  };
}

/**
 * @module Actions/User
 * @desc User Actions
 */
import { ActionTypes } from 'constants/index';

export function selectDatabaseRow(rowIndex: String): Object {
  return {
    type: ActionTypes.DB_SELECT_ROW,
    payload: rowIndex
  }
}

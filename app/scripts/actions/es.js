import { ActionTypes } from 'constants/index';

export function searchElastic(url): Object {
  return {
    type: ActionTypes.ES_SEARCH_REQUEST,
    payload: url
  }
}

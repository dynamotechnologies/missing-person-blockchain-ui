/**
 * Created by patrickjang on 4/14/18.
 */
// @flow
/**
 * @module Actions/User
 * @desc User Actions
 */
import { ActionTypes } from 'constants/index';

/**
 * Upload to IPFS
 *
 * @param {string} hash
 * @returns {Object}
 */
export function uploadToIPFS(hash: string): Object {
  return {
    type: ActionTypes.IPFS_UPLOAD_REQUEST,
    payload: { hash },
  };
}

// @flow
/**
 * @module Actions/Web3
 * @desc Web3 Actions
 */
import { ActionTypes } from 'constants/index';

export function unlockAccount(): Object {
  return {
    type: ActionTypes.WEB3_UNLOCK_ACCOUNT_REQUEST,
    payload: {},
  };
}

export function sendTransaction(data: Object): Object {
  return {
    type: ActionTypes.WEB3_SEND_TRANSACTION_REQUEST,
    payload: {},
  };
}

export function sendToDataRegistry(data: Object): Object {
  return {
    type: ActionTypes.WEB3_SEND_TO_DATA_REGISTRY_REQUEST,
    payload: data
  }
}

export function getTopDataRegistry(): Object {
  return {
    type: ActionTypes.WEB3_GET_TOP_DATA_REGISTRY_REQUEST,
    payload: {}
  }
}

export function getContractData(num: Number): Object {
  return {
    type: ActionTypes.WEB3_GET_CONTRACT_DATA_REQUEST,
    payload: num
  }
}

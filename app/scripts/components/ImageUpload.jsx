import React from 'react';
import "../../styles/components/Uploads.scss";

const ImageUpload = () => (
	<div className="upload-container">
		<div className="upload-main">
		    <img className="facial-recognition-icon" src={require('../../../assets/media/icons/facial-recognition-image-loading.svg')}/>
		    <img className="upload-icon" src={require('../../../assets/media/icons/file-upload.svg')}/>
		</div>
		<div className="loading">
	    	<img className="loading-icon" src={require('../../../assets/media/icons/loading-icon.svg')}/>
		</div>	
	</div>
);

export default ImageUpload;

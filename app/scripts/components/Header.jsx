import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';

import { logOut, uploadToIPFS } from 'actions';

export default class Header extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    headerName: PropTypes.string
  };

  handleClickLogout = e => {
    e.preventDefault();
    const { dispatch } = this.props;

    dispatch(logOut());
  };

  handleUpload = e => {
    e.preventDefault();
    const { dispatch } = this.props;
    // make data dynamic and move to RegistryDirectory.jsx
    dispatch(uploadToIPFS('87156429296605213895040f31e2defd'))
  }

  render() {
    const {headerName} = this.props;

    return (
      <header className="app__header">
        <div className="app__container">
          <h1 className="app__header__name">{headerName || "Header Name"}</h1>
          <div className="app__header__menu">
            <Button onClick={this.handleUpload} color="warning" outline size="lg" className="float-right ml-10">Upload</Button>
            <Button color="warning" size="lg" className="float-right"> + Add New</Button>
          </div>
        </div>
      </header>
    );
  }
}

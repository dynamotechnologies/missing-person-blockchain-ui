import React from 'react';
import "../../styles/components/Uploads.scss";

const FingerprintsUpload = () => (
  	<div className="upload-container">
		<div className="upload-main">
		    <img className="fingerprint-icon" src={require('../../../assets/media/icons/fingerprint-image-loading.svg')}/>
		    <img className="upload-icon" src={require('../../../assets/media/icons/file-upload.svg')}/>
		</div>
		<div className="loading">
	    	<img className="loading-icon" src={require('../../../assets/media/icons/loading-icon.svg')}/>
		</div>	
	</div>
);

export default FingerprintsUpload;

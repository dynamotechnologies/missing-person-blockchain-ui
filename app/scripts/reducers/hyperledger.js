/**
 * Created by patrickjang on 4/13/18.
 */
import immutable from 'immutability-helper';
import { createReducer } from 'modules/helpers';
import { parseError } from 'modules/client';
import { ActionTypes } from 'constants/index';

export const hyperledgerState = {
  directory: [],
  isLoading: false
};

export default {
  hyperledger: createReducer(hyperledgerState, {
    [ActionTypes.HYPERLEDGER_GET_DIRECTORY_REQUEST](state, { payload }) {
      return immutable(state, {
        directory: { $set: [] },
        isLoading: { $set: true }
      });
    },
    [ActionTypes.HYPERLEDGER_GET_DIRECTORY_SUCCESS](state, { payload }) {
      return immutable(state, {
        directory: { $set: payload.data || [] },
        isLoading: { $set: false }
      });
    },
    [ActionTypes.HYPERLEDGER_GET_DIRECTORY_FAILURE](state, { payload }) {
      return immutable(state, {
        directory: { $set: [] },
        isLoading: { $set: false }
      });
    },
  }),
};

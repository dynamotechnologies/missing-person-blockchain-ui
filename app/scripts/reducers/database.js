import immutable from 'immutability-helper';
import { createReducer } from 'modules/helpers';
import { parseError } from 'modules/client';
import { ActionTypes } from 'constants/index';

export const dbState = {
  directory: [],
  isLoading: false
};

export default {
  db: createReducer(dbState, {
    [ActionTypes.HYPERLEDGER_GET_DIRECTORY_REQUEST](state, { payload }) {
      return immutable(state, {
        directory: { $set: [] },
        isLoading: { $set: true }
      });
    },
    [ActionTypes.HYPERLEDGER_GET_DIRECTORY_SUCCESS](state, { payload }) {
      return immutable(state, {
        directory: { $set: payload.data || [] },
        isLoading: { $set: false }
      });
    },
    [ActionTypes.HYPERLEDGER_GET_DIRECTORY_FAILURE](state, { payload }) {
      return immutable(state, {
        directory: { $set: [] },
        isLoading: { $set: false }
      });
    },
    [ActionTypes.ES_SEARCH_REQUEST](state, { payload }) {
      return immutable(state, {
        directory: { $set: [] },
        isLoading: { $set: true }
      });
    },
    [ActionTypes.ES_SEARCH_SUCCESS](state, { payload }) {
      console.log(payload.data);
      const data = payload.data;
      const hits = data.hits && data.hits.hits;

      const directory = hits.map((hit) => hit._source.data);


      return immutable(state, {
        directory: { $set: directory || [] },
        isLoading: { $set: false }
      });
    },
    [ActionTypes.ES_SEARCH_FAILURE](state, { payload }) {
      return immutable(state, {
        directory: { $set: [] },
        isLoading: { $set: false }
      });
    },
  }),
};

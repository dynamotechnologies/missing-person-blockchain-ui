/**
 * Created by patrickjang on 4/13/18.
 */
import immutable from 'immutability-helper';
import { createReducer } from 'modules/helpers';
import { parseError } from 'modules/client';
import { ActionTypes } from 'constants/index';

export const web3State = {
  contracts: [],
  isLoading: false
};

export default {
  web3: createReducer(web3State, {
    [ActionTypes.WEB3_GET_CONTRACT_DATA_REQUEST](state, { payload }) {
      return immutable(state, {
        isLoading: { $set: true }
      });
    },
    [ActionTypes.WEB3_GET_CONTRACT_DATA_SUCCESS](state, { payload }) {
      return immutable(state, {
        contracts: { $push: [payload] || [] },
        isLoading: { $set: false }
      });
    },
    [ActionTypes.WEB3_GET_CONTRACT_DATA_FAILURE](state, { payload }) {
      return immutable(state, {
        isLoading: { $set: false }
      });
    },
  }),
};

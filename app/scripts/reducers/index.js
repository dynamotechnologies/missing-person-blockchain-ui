import app from './app';
import github from './github';
import hyperledger from './hyperledger';
import user from './user';
import db from './database';
import web3 from './web3';

export default {
  ...app,
  ...db,
  ...github,
  ...user,
  ...hyperledger,
  ...web3
};

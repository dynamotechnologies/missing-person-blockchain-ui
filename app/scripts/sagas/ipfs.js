/**
 * Created by patrickjang on 4/14/18.
 */
/**
 * @module Sagas/GitHub
 * @desc GitHub
 */

import { all, call, put, takeLatest } from 'redux-saga/effects';
import { request } from 'modules/client';
import { fs } from 'fs';
import ipfsAPI from 'ipfs-api';

import { ActionTypes, S3_ENDPOINT } from 'constants/index';

/**
 * IPFS
 *
 * @param {Object} payload
 *
 */
export function* upload({ payload }) {
  try {
    const { hash } = payload;

    const ipfs = ipfsAPI('localhost', '5001', {protocol: 'http'}) // leaving out the arguments will default to these values

    const [face, fingerprint, person] = yield all([
      call(ipfs.util.addFromURL, `${S3_ENDPOINT}/${hash}/face.png`),
      call(ipfs.util.addFromURL, `${S3_ENDPOINT}/${hash}/fingerprint.png`),
      call(ipfs.util.addFromURL, `${S3_ENDPOINT}/${hash}/person.json`)
    ])

    yield put({
      type: ActionTypes.IPFS_UPLOAD_SUCCESS,
      payload: { face, fingerprint, person },
    });

  }
  catch (err) {
    yield put({
      type: ActionTypes.IPFS_UPLOAD_FAILURE,
      payload: err,
    });
  }
}

/**
 * GitHub Sagas
 */
export default function* root() {
  yield all([
    takeLatest(ActionTypes.IPFS_UPLOAD_REQUEST, upload),
  ]);
}

/**
 * Created by patrickjang on 4/13/18.
 */
/**
 * @module Sagas/GitHub
 * @desc GitHub
 */

import { all, call, put, takeLatest } from 'redux-saga/effects';
import { request } from 'modules/client';

import { ActionTypes } from 'constants/index';

/**
 * Login
 *
 * @param {Object} payload
 *
 */
export function* getDirectory({ payload }) {
  try {
    const response = yield call(request, payload.endpoint);
    yield put({
      type: ActionTypes.HYPERLEDGER_GET_DIRECTORY_SUCCESS,
      payload: { data: response },
    });
  }
  catch (err) {
    /* istanbul ignore next */
    yield put({
      type: ActionTypes.HYPERLEDGER_GET_DIRECTORY_FAILURE,
      payload: err,
    });
  }
}

/**
 * GitHub Sagas
 */
export default function* root() {
  yield all([
    takeLatest(ActionTypes.HYPERLEDGER_GET_DIRECTORY_REQUEST, getDirectory),
  ]);
}

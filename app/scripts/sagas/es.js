import { all, call, put, takeLatest } from 'redux-saga/effects';
import { request } from 'modules/client';

import { ActionTypes } from 'constants/index';
import {getDirectory} from "./hyperledger";

export function* searchElastic({ payload }) {
  try {
    const response = yield call(request, payload.url);
    yield put({
      type: ActionTypes.ES_SEARCH_SUCCESS,
      payload: { data: response },
    });
  }
  catch (err) {
    yield put({
      type: ActionTypes.ES_SEARCH_FAILURE,
      payload: err,
    });
  }
}

export default function* root() {
  yield all([
    takeLatest(ActionTypes.ES_SEARCH_REQUEST, searchElastic),
  ]);
}

import { all, fork } from 'redux-saga/effects';

import es from './es';
import github from './github';
import hyperledger from './hyperledger';
import user from './user';
import ipfs from './ipfs';
import web3 from './web3';

/**
 * rootSaga
 */
export default function* root() {
  yield all([
    fork(es),
    fork(github),
    fork(hyperledger),
    fork(user),
    fork(ipfs),
    fork(web3)
  ]);
}

import Web3 from 'web3';
import contract from "truffle-contract";
import {BigNumber} from "bignumber.js"; 
import bs58 from "bs58";
import { all, call, put, takeLatest } from 'redux-saga/effects';

import { ActionTypes, WEB_3, DATA_REGISTRY, CATEGORY_NAME_MAP } from 'constants/index';

const provider = new Web3.providers.HttpProvider(WEB_3.provider);
let web3 = new Web3(provider);
const MyContract = contract(DATA_REGISTRY);

MyContract.setProvider(provider); // => a promise
MyContract.setNetwork('22392');

const ByteArraytoHexString = (byteArray) => {
  return Array.from(byteArray, function(byte) {
    return ('0' + (byte & 0xFF).toString(16)).slice(-2);
  }).join('')
}

const fromIPFSHash = hash => {
   var bytes = bs58.decode(hash);
   var multiHashId = 2;
   // remove the multihash hash id
   var hexval = ByteArraytoHexString(bytes.slice(multiHashId, bytes.length));
   return new BigNumber("0x" + hexval);
};

const toIPFSHash = str => {
    const remove0x = str.slice(2, str.length);
    // add back the multihash id
    const bytes = Buffer.from(`1220${remove0x}`, "hex");
    const hash = bs58.encode(bytes);
    return hash;
};

const getContractDataRequest = (num) => {
  return MyContract.deployed().then(instance => {
    
    return instance.getData.call(num);
  }).then(result => {
    return {
      ipfs: toIPFSHash(result[0].toString()),
      creationts: new Date(result[1].toNumber()),
      reporter: result[3] || '',
      category: CATEGORY_NAME_MAP[result[4].toNumber()],
      complete: result[5],
      mimetype: result[6],
    }
  }, err => {
    console.error(err);
    throw err
  })
}

const getTopRequest = () => {
  return MyContract.deployed().then(function(instance) {
    return instance.getTop.call()
  }).then(function(result) {
    return result.toNumber()
  }, err => {throw err});
}

const unlockAccountRequest = (account, password) => {
  const unlocked = web3.personal.unlockAccount(account, password, 0);
  return unlocked
}

const sendTransactionRequest = data => {
  const transactionValue = {from: WEB_3.account, to: WEB_3.contract, value: 100 }
  return web3.eth.sendTransaction(transactionValue)
    .then(receipt => receipt, err => {throw err});
}

const sendToDataRegistryRequest = data => {
  const {ipfs, creationts, category} = data;
  return MyContract.deployed().then(function(instance) {
    const ipfsToSend = web3.toBigNumber(web3.toHex(ipfs));
    const creationtsToSend = web3.toBigNumber(web3.toHex(creationts));
    const categoryToSend = web3.toBigNumber(category);
    const mimetype = 'application/json';
    return instance.addData(ipfsToSend, creationtsToSend, categoryToSend, mimetype, {from: WEB_3.account, gas: WEB_3.gas })
  }).then(function(result) {
    return result
  }, err => {throw err});
}

export function* sendTransaction(data) {
  try {
    const response = yield call(sendTransactionRequest, data);
    if(response) {
      yield put({
        type: ActionTypes.WEB3_SEND_TRANSACTION_SUCCESS,
        payload: response,
      });
    } else {
      yield put({
        type: ActionTypes.WEB3_SEND_TRANSACTION_FAILURE,
        payload: response,
      });
    }
  } catch(err) {
    yield put({
      type: ActionTypes.WEB3_SEND_TRANSACTION_FAILURE,
      payload: err,
    });
  }
}

export function* sendToDataRegistry({payload}) {
  try {
    const response = yield call(sendToDataRegistryRequest, payload);
    yield put({
      type: ActionTypes.WEB3_SEND_TO_DATA_REGISTRY_SUCCESS,
      payload: response,
    });
  } catch(err) {
    yield put({
      type: ActionTypes.WEB3_SEND_TO_DATA_REGISTRY_FAILURE,
      payload: err,
    });
  }
}

export function* getTopDataRegistry() {
  try {
    const response = yield call(getTopRequest);
    yield put({
      type: ActionTypes.WEB3_GET_TOP_DATA_REGISTRY_SUCCESS,
      payload: response,
    });
  } catch (err) {
    yield put({
      type: ActionTypes.WEB3_GET_TOP_DATA_REGISTRY_FAILURE,
      payload: err
    })
  }
}

export function* getContractData({payload}) {
  try {
    const response = yield call(getContractDataRequest, payload);
    yield put({
      type: ActionTypes.WEB3_GET_CONTRACT_DATA_SUCCESS,
      payload: response,
    });
  } catch (err) {
    yield put({
      type: ActionTypes.WEB3_GET_CONTRACT_DATA_FAILURE,
      payload: err
    })
  }
}

export function* unlockAccount() {
  try {
    const response = yield call(unlockAccountRequest, WEB_3.account, WEB_3.password)
    yield put({
      type: ActionTypes.WEB3_UNLOCK_ACCOUNT_SUCCESS,
      payload: { data: response}
    })
  } catch(err) {
    yield put({
      type: ActionTypes.WEB3_UNLOCK_ACCOUNT_FAILURE,
      payload: err,
    });
  }
}

const transformDataToPass = (category, ipfsHash, ) => {
  const ipfsHashToPass = fromIPFSHash(ipfsHash)
  return {
    ipfs: ipfsHashToPass,
    creationts: new Date().getTime(),
    category
  }
}

export function* triggerUploadPerson({payload}) {
  const {face, fingerprint, person} = payload;
  console.log('perosn', person)
  try {

    console.log(transformDataToPass(5, person[0].hash));
    const personUpload = yield(call(sendToDataRegistryRequest, transformDataToPass(5, person[0].hash)));
    // const [faceUpload, fingerprintUpload, personUpload] = yield({
    //   call(sendToDataRegistryRequest, transformDataToPass(2, face)),
    //   call(sendToDataRegistryRequest, transformDataToPass(1, fingerprint)),
    //   call(sendToDataRegistryRequest, transformDataToPass(5, person))
    // })

    yield put({
      type: ActionTypes.WEB3_BATCH_UPLOAD_PERSON_SUCCESS,
      payload: { personUpload}
    })
  } catch(err) {
    yield put({
      type: ActionTypes.WEB3_BATCH_UPLOAD_PERSON_FAILURE,
      payload: err,
    })
  }
}

export default function* root() {
  yield all([
    takeLatest(ActionTypes.WEB3_SEND_TRANSACTION_REQUEST, sendTransaction),
    takeLatest(ActionTypes.WEB3_UNLOCK_ACCOUNT_REQUEST, unlockAccount),
    takeLatest(ActionTypes.WEB3_SEND_TO_DATA_REGISTRY_REQUEST, sendToDataRegistry),
    takeLatest(ActionTypes.WEB3_GET_TOP_DATA_REGISTRY_REQUEST, getTopDataRegistry),
    takeLatest(ActionTypes.WEB3_GET_CONTRACT_DATA_REQUEST, getContractData),
    takeLatest(ActionTypes.IPFS_UPLOAD_SUCCESS, triggerUploadPerson)
  ]);
}

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import Web3 from 'web3';

import Loader from 'components/Loader';
import '../../styles/components/RegistryDirectory.scss'

import { getDirectory, unlockAccount, sendToDataRegistry } from '../actions';
import { COUNTRIES, WEB_3 } from '../constants';
import contract from "truffle-contract";

export class RegistryDirectory extends React.Component {
  state = {
    query: 'react',
    registrants: [
      {
        name: 'Claudia Chapman',
        age: 47,
        description: "5'11 White Male",
        location: 'Arlington, VA',
        fingerprint: 'Valid Fingerprint'
      },
      {
        name: 'Barry Figueroa',
        age: 35,
        description: "5'5 Asian Female",
        location: 'Arlington, VA',
        fingerprint: 'Valid Fingerprint',
        selected: false
      },
      {
        name: 'Genevieve Rogers',
        age: 42,
        description: "5'8 White Female",
        location: 'Arlington, VA',
        fingerprint: 'Valid Fingerprint'
      },
      {
        name: 'Alexander Webb',
        age: 29,
        description: "5'6 White Female",
        location: 'Arlington, VA',
        fingerprint: 'Valid Fingerprint'
      },
      {
        name: 'Marcus Hogan',
        age: 65,
        description: "5'9 Latino Male",
        location: 'Arlington, VA',
        fingerprint: 'Valid Fingerprint'
      }
    ],
    tableHeaders: ['Name', 'Age', 'Height/Sex', 'Location', 'Fingerprint']
  };

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
  };

  handleRowClick = e => {
    e.preventDefault();
    const {dispatch} = this.props;

    const data = {
      ipfs: '87156429296605213895040f31e2defd',
      creationts: new Date().getTime(),
      category: Math.floor(Math.random() * Math.floor(max))
    };
    dispatch(sendToDataRegistry(data))
  }

  componentDidMount() {
    const { query } = this.state;
    const { dispatch } = this.props;

    dispatch(getDirectory(COUNTRIES.CHINA))
    dispatch(unlockAccount())
  }

  render() {
    const { query } = this.state;
    const { hyperledger: directory } = this.props;
    let output;
    return (
      <div key="RegistryDirectory">
        <table className="registrant-table">
          <thead>
            <tr>
              {this.state.tableHeaders.map((header) => {
                return <th key={header}>
                          <span>{header}</span>
                          <img className="header-sort-icon" src={require(`../../../assets/media/icons/sort-icon.svg`)}/>
                        </th>
              })}
            </tr>
            <tr className="header-bottom-bar">
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </thead>
          <tbody><tr className="spacer1"></tr></tbody>
          {this.state.registrants.map((registrant) => {
            return <RegistrantCard 
              registrant={registrant} 
              columns={this.state.tableColumns} 
              key={registrant.name} 
              handleRowClick={this.handleRowClick}
            />
          })}
        </table>
      </div>
    );
  }
}

export class RegistrantCard extends React.Component {
  render() {
    const {registrant, handleRowClick } = this.props;

    return (
      <tbody>
        <tr className="spacer2"></tr>
        <tr className="registrant-row" onClick={handleRowClick}>
          <td className="registrant-data">
            <img className="registrant-image" src={require(`../../../assets/media/icons/icons8-person-female-filled-100.png`)}/>
            {registrant.name}
          </td>
          <td className="registrant-data">
            <img className="column-icons" src={require(`../../../assets/media/icons/age-icon.svg`)}/>
            {registrant.age}
          </td>
          <td className="registrant-data">
            <img className="column-icons" src={require(`../../../assets/media/icons/stats-icon.svg`)}/>
            {registrant.description}
          </td>
          <td className="registrant-data">
            <img className="column-icons" src={require(`../../../assets/media/icons/place-icon.svg`)}/>
            {registrant.location}
          </td>
          <td className="registrant-data">
            <img className="column-icons" src={require(`../../../assets/media/icons/fingerprint-icon.svg`)}/>
            {registrant.fingerprint}
          </td>
        </tr>
        { registrant.selected &&
          <tr className="more-info"><td colSpan='12'>More Info...</td></tr>
        }
      </tbody>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return { hyperledger: state.hyperledger };
}

export default connect(mapStateToProps)(RegistryDirectory);

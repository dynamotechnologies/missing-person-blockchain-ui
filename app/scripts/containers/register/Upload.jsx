import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import ImageUpload from 'components/ImageUpload';
import FingerprintsUpload from 'components/FingerprintsUpload';

export class Upload extends React.Component {
  state = {
    query: 'react',
  };

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired
  };

  componentDidMount() {
    const { query } = this.state;
    const { dispatch } = this.props;
  }

  render() {
    const {type} = this.props;
    const { query } = this.state;
    return (
      <div key="Upload" className="app__github">
        <div className="app__github__selector">
          <div role="group" aria-label="Basic example">
            Upload
            {type === "fingerprint" && <FingerprintsUpload/>}
            {type === "image" && <ImageUpload/>}
          </div>
        </div>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return { };
}

export default connect(mapStateToProps)(Upload);

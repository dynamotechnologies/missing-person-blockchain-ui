import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import cx from 'classnames';
import history from 'modules/history';
import RoutePublic from 'modules/RoutePublic';
import RoutePrivate from 'modules/RoutePrivate';

import config from 'config';
import { showAlert } from 'actions';

import {List, Scan, NotFound, Register, Start, Database} from 'routes';

import Header from 'components/Header';
import SystemAlerts from 'components/SystemAlerts';

export class App extends React.Component {
  static propTypes = {
    app: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
  };

  render() {
    const { app, dispatch, user } = this.props;

    return (
      <ConnectedRouter history={history}>
        <div
          className="app--private"
        >
          <Helmet
            defer={false}
            htmlAttributes={{ lang: 'en-us' }}
            encodeSpecialCharacters={true}
            defaultTitle={config.title}
            titleTemplate={`%s | ${config.name}`}
            titleAttributes={{ itemprop: 'name', lang: 'en-us' }}
          />
          <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
            <Link to="/" className="navbar-brand">
              <img src={require(`assets/media/images/dynamo.png`)} width="250px"/>
            </Link>
          </nav>
            <Switch>
              <RoutePublic path="/" exact component={Start} headerName="Start"/>
              <RoutePublic path="/database/:id" exact component={Database} headerName="List"/>
              <RoutePublic path="/matches" exact component={List} container="matches" headerName="Potential Matches"/>
              <RoutePublic path="/scan-image" exact component={Scan} type="image" headerName="Scan Image"/>
              <RoutePublic path="/scan-fingerprint" exact component={Scan} type="fingerprint" headerName="Scan Fingerprint"/>
              <RoutePublic path="/register-missing" exact component={Register} type="missing" headerName="Register New Person"/>
              <RoutePublic path="/register-new" exact component={Register} type="new" headerName="Register New Person"/>
              <RoutePublic path="/register-pii" exact component={Register} type="pii" headerName="Register New Person"/>
              <RoutePublic path="/register-upload-image" exact component={Register} type="image" headerName="Register New Person"/>
              <RoutePublic path="/register-upload-fingerprint" exact component={Register} type="fingerprint" headerName="Register New Person"/>
              <RoutePublic path="/register-success" exact component={Register} type="success" headerName="Register New Person"/>
              <Route component={NotFound} />
            </Switch>
          <SystemAlerts alerts={app.alerts} dispatch={dispatch} />
        </div>
      </ConnectedRouter>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    app: state.app,
    user: state.user,
  };
}

export default connect(mapStateToProps)(App);

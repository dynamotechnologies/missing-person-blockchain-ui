import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import Loader from 'components/Loader';
import '../../styles/components/PotentialMatches.scss'

export class PotentialMatches extends React.Component {
  state = {
    query: 'react',
    matchList: [
      { name: 'Barry Figuerola',
        status: 'Missing',
        age: 35,
        description: "5'5 Asian Female",
        cityState: 'Arlington, VA',
        validFingerprint: true
      },
      { name: 'Rhoda Park',
        status: 'Missing',
        age: 35,
        description: "5'5 Asian Female",
        cityState: 'Falls Church, VA',
        validFingerprint: true
      },
      { name: 'Jean Chang',
        status: 'Missing',
        age: 38,
        description: "5'7 Asian Female",
        cityState: 'Arlington, VA',
        validFingerprint: true
      },
      { name: 'Eula Montgomery',
        status: 'Missing',
        age: 32,
        description: "5'3 Asian Female",
        cityState: 'Fairfax, VA',
        validFingerprint: true
      },
      { name: 'Fannie Vega',
        status: 'Missing',
        age: 19,
        description: "5'5 Asian Female",
        cityState: 'Arlington, VA',
        validFingerprint: true
      }
    ]
  };

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { query } = this.state;
    const { dispatch } = this.props;
  }

  render() {
    const { query } = this.state;
    let output;
    return (
      <div key="PotentialMatches">
        <div>
          <div className="user-card-list">
            {this.state.matchList.map((user) => {
              return <UserCard user={user} key={user.name}/>
            })}
          </div>
        </div>
      </div>
    );
  }
}

export class UserCard extends React.Component {
  render() {
    const fingerprint = this.props.user.validFingerprint ? (
        <li>Valid Fingerprint</li>
      ) : (
        <li>Invalid Fingerprint</li>
      );

    return (
      <div className="user-card">
        <div className="top-bar"></div>
        <img className="user-image" src={require(`../../../assets/media/icons/icons8-person-female-filled-100.png`)}/>
        <div className="user-name">{this.props.user.name}</div>
        <div className="user-status">{this.props.user.status}</div>
        <div className="icon-row">
          <a className="icon-btn" href="#"><img src={require(`../../../assets/media/icons/export-icon.svg`)}/></a>
          <a className="icon-btn" href="#"><img src={require(`../../../assets/media/icons/missing-icon.svg`)}/></a>
          <a className="icon-btn" href="#"><img src={require(`../../../assets/media/icons/add-to-icon.svg`)}/></a>
        </div>
        <ul>
          <li>{this.props.user.age} years old</li>
          <li>{this.props.user.description}</li>
          <li>{this.props.user.cityState}</li>
          {fingerprint}
        </ul>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return { };
}

export default connect(mapStateToProps)(PotentialMatches);

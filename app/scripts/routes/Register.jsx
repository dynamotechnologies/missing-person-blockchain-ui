import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import Header from 'components/Header';
import {Upload} from 'containers'; 

export class Register extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    type: PropTypes.string,
    headerName: PropTypes.string
  };

  render() {
    const { dispatch, container, headerName, type } = this.props;
    const isUpload = type === "image" || type === "fingerprint";
    return (
      <div>
        <Header dispatch={dispatch} headerName={headerName} />
        <main className="app__main">
        <div key="Register" className="app__private app__route">
          <div className="app__container">
              <div className="app__private__header">
                <h1>{`${headerName} - ${type}`}</h1>
              </div>
              {isUpload && <Upload type={type}/>}
          </div>
        </div>
        </main>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return { user: state.user };
}

export default connect(mapStateToProps)(Register);

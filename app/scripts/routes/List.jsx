import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import Header from 'components/Header';
import {RegistryDirectory, PotentialMatches} from 'containers';

export class List extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    container: PropTypes.string,
    headerName: PropTypes.string
  };

  render() {
    const { dispatch, container, headerName } = this.props;

    return (
      <div>
        <Header dispatch={dispatch} headerName={headerName} />
        <main className="app__main">
        <div key="List" className="app__private app__route">
          <div className="app__container">
              <div className="app__private__header">
                <h1>Search</h1>
              </div>
              {container === "registry" && <RegistryDirectory {...this.props} />}
              {container === "matches" && <PotentialMatches {...this.props} />}
          </div>
        </div>
        </main>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return { user: state.user };
}

export default connect(mapStateToProps)(List);

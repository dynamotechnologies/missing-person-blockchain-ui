import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

import { getDirectory, unlockAccount, sendToDataRegistry, getTopDataRegistry, getContractData, searchElastic, uploadToIPFS } from '../actions';
import { organizations } from "../constants";
import {RegistryDirectory, PotentialMatches} from 'containers';
import {ListGroup, ListGroupItem} from 'reactstrap';
import {Card, CardHeader, CardTitle, CardBody, CardImg} from 'reactstrap';
import {Input, InputGroup, InputGroupAddon, InputGroupText, InputGroupButton} from 'reactstrap';
import {Button} from 'reactstrap';

export class Database extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    container: PropTypes.string,
    headerName: PropTypes.string
  };

  componentDidMount() {
    const {dispatch, match} = this.props;
    const { id } = match.params;
    const org = organizations[id];

    if (org.details.chain) {
      if (org.details.chain.type == 'Ethereum') {
        dispatch(searchElastic(org.details.chain.elastic));
      } else {
        dispatch(getDirectory(org.details.chain));
      }
    }
    dispatch(getTopDataRegistry())
    dispatch(getContractData(30))
  }

  handleSearch(event) {
    this.setState({
      filter: event.currentTarget.value
    });
  }

  handleOnClick(index, person ) {
    this.setState({
      selectedRow: index
    });
  }

  handleOnClickMissing(person) {
    const {dispatch} = this.props;

    dispatch(uploadToIPFS(person.hash));
  }

  render() {
    const { dispatch, container, headerName, match } = this.props;
    const { id } = match.params;

    const {hyperledger} = this.props;
    let directory = hyperledger.directory;

    if (this.state && this.state.filter) {
      directory = directory.filter((person) => {
        let matches = false;

        for (var k in person) {
          if (`${person[k]}`.toLowerCase().indexOf(this.state.filter.toLowerCase()) > -1) {
            matches = true;
          }
        }

        return matches;
      })
    }

    return (

      <div className="container mt-5">

        <InputGroup className="mb-5">
          <InputGroupAddon addonType="append">
            <InputGroupText><FontAwesomeIcon icon="search" size="lg"/></InputGroupText>
          </InputGroupAddon>
          <Input onChange={this.handleSearch.bind(this)} placeholder="Filter Persons"/>
        </InputGroup>

        <ListGroup className="person-group">
          {
            directory ? directory.map((person, index) => <PersonRow 
                key={index} 
                person={person} 
                isSelected={index == (this.state && this.state.selectedRow)} 
                onClick={() => {this.handleOnClick(index, person)}}
                onClickMissing={() => this.handleOnClickMissing(person)}
              />) : ''
          }
        </ListGroup>
      </div>
    );
  }
}

export class PersonRow extends React.PureComponent {
  render() {
    const {person, isSelected, onClickMissing} = this.props;

    const {firstName, lastName, age, city, state} = person;
    const {sex, height, weight, hairColor, eyeColor, ethnicity} = person;
    const {dateOfBirth, nationality, passportNumber, additionalId, additionalIdNumber} = person;
    const {streetAddress, postalCode, phoneNumber, countryCode} = person;

    const localizedHeightFeet = parseInt(height / 12);
    const localizedHeightInches = height % 12;
    return (
      <React.Fragment>
        {isSelected ? <ListGroupItem className="profile mb-4" onClick={this.props.onClick}>
              <div className="row no-gutters">
                <div className="col-2 border-right">
                  <Card>
                    <CardImg className="text-center p-3" top tag="div">
                      <FontAwesomeIcon icon="user-circle" size="4x"/>
                    </CardImg>
                    <CardBody>
                      <CardTitle className="text-center">{lastName}, {firstName}</CardTitle>
                      <div className="row no-gutters">
                        <div className="col-4 text-right">
                          <Button size="sm" className="pl-3 pr-3"><FontAwesomeIcon className="fa-fw" icon="share-alt"/></Button>
                        </div>
                        <div className="col-4 text-center">
                          <Button size="sm" className="pl-3 pr-3 btn-outline-danger" alt="Mark Missing" onClick={onClickMissing}><FontAwesomeIcon className="fa-fw" icon="flag" /></Button>
                        </div>
                        <div className="col-4 text-left">
                          <Button size="sm" className="pl-3 pr-3"><FontAwesomeIcon className="fa-fw" icon="file-alt"/></Button>
                        </div>
                      </div>
                    </CardBody>
                  </Card>
                  <ProfileCard title="Overview" titleIcon="list-alt" items={[
                    {icon: 'calendar-alt', value: `${age} years old`},
                    {icon: 'male', value: `${localizedHeightFeet}' ${localizedHeightInches}'' ${ethnicity} ${sex}`},
                    {icon: 'map-marker-alt', value: `${city}, ${state}`, valueClassName: 'text-capitalize'}
                  ]} />
                </div>
                <div className="col-4 border-right">
                  <ProfileCard title="Physical" titleIcon="street-view" dtCols={4} items={[
                    {text: 'Sex', value: sex},
                    {text: 'Height', value: height},
                    {text: 'Weight', value: weight},
                    {text: 'Hair Color', value: hairColor},
                    {text: 'Eye Color', value: eyeColor},
                    {text: 'Ethnicity', value: ethnicity}
                  ]} />

                  <ProfileCard title="Identification" titleIcon="id-card" dtCols={4} items={[
                    {text: 'Date of Birth', value: dateOfBirth},
                    {text: 'Nationality', value: nationality},
                    {text: 'Passport', value: passportNumber},
                    {text: 'Add. ID', value: additionalId},
                    {text: 'Add. ID #', value: additionalIdNumber}
                  ]} />
                </div>
                <div className="col-4 border-right">
                  <ProfileCard title="Known Contact Information" titleIcon="mobile-alt" dtCols={4} items={[
                    {text: 'Address', value: streetAddress},
                    {text: 'City, State Zip', value: `${city}, ${state} ${postalCode}`, valueClassName: 'text-capitalize'},
                    {text: 'Phone #', value: `${countryCode} ${phoneNumber}`}
                  ]} />

                  <ProfileCard title="Notes" titleIcon="sticky-note" dtCols={4} items={[

                  ]} />

                </div>
                <div className="col-2">
                  <ProfileCard title="History" titleIcon="history" items={[

                  ]} />
                </div>
              </div>
            </ListGroupItem>
            :
            <ListGroupItem className="profile-condensed mb-4" onClick={this.props.onClick}>
              <div className="row">
                <div className="col-1 text-center"><FontAwesomeIcon icon="user-circle" size="3x"/></div>
                <div className="col pt-3"><span className="align-middle">{lastName}, {firstName}</span></div>
                <div className="col pt-3"><FontAwesomeIcon className="mr-2" icon="calendar-alt"/> {age} years old</div>
                <div className="col pt-3"><FontAwesomeIcon className="mr-2" icon="male"/> {localizedHeightFeet}' {localizedHeightInches}'' {ethnicity} {sex}
                </div>
                <div className="col pt-3 text-capitalize"><FontAwesomeIcon className="mr-2"
                                                                           icon="map-marker-alt"/> {city}, {state}</div>
                <div className="col-1 pt-3"></div>
              </div>
            </ListGroupItem>
        }
      </React.Fragment>
    )
  }
}

class ProfileCard extends React.PureComponent {

  render() {
    const {title, titleIcon, items, dtCols = 3} = this.props;
    const ddCols = 12 - dtCols;

    return (
      <Card>
        <CardHeader>
          <CardTitle><span><FontAwesomeIcon className="mr-2" icon={titleIcon} /> {title}</span></CardTitle>
        </CardHeader>
        <CardBody>
          <dl className="dl-horizontal row no-gutters">
            {items.map((item, index) => (
              <React.Fragment key={index}>
                <dt className={`col-${dtCols} text-${item.icon ? 'center' : 'right pr-4'}`}>{item.icon ? <FontAwesomeIcon icon={item.icon}/> : item.text}</dt>
                <dd className={`col-${ddCols} ${item.valueClassName || ''}`}>
                  {item.value}
                </dd>
              </React.Fragment>
            ))}
            </dl>
        </CardBody>
      </Card>
    )
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return { user: state.user, hyperledger: state.db};
}

export default connect(mapStateToProps)(Database);

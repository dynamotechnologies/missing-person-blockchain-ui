import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';
import SVG from 'react-inlinesvg';

import {CardDeck, Card, CardImg, CardHeader, CardText, CardImgOverlay, CardTitle, CardBody} from 'reactstrap';
import {organizations} from '../constants';

export class Start extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    headerName: PropTypes.string
  };

  state = {
    organizations
  }

  render() {
    const { dispatch, headerName } = this.props;

    return (
      <div>
        <div className="container-fluid p-5">
          <CardDeck className="start-card">
            {Object.keys(this.state.organizations).map((key, index) => {
              const org = this.state.organizations[key];
              return <OrgCard {...org} key={org.id} dispatch={dispatch} index={index}></OrgCard>
            })}
          </CardDeck>
        </div>
      </div>

    );
  }
}

export class OrgCard extends React.PureComponent {
  static propTypes = {
    name: PropTypes.string

  };

  handleOnClick = () => {
    this.props.dispatch(push('/database/' + this.props.id));
  }

  render() {
    const {name, logo, details} = this.props;

    return (
      <Card className="mb-4 text-white bg-dark font-weight-bold" onClick={this.handleOnClick}>
        <CardHeader>
          <CardTitle className="text-center">{name}</CardTitle>
        </CardHeader>
        {logo ? <CardImg className="org-card-logo img-fluid" src={logo} /> : ''}
        <CardBody>

          <CardText className="text-success mt-5" tag="div">
            <OrgCardDetails details={details} />
          </CardText>
        </CardBody>
      </Card>
    )
  }

}

export class OrgCardDetails extends React.PureComponent {
  static propTypes = {
    details: PropTypes.object
  };

  render() {
    const {details} = this.props;
    return (
      <pre>
        {details ? JSON.stringify(details, undefined, 2) : ''}
      </pre>
    )
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return { user: state.user };
}

export default connect(mapStateToProps)(Start);

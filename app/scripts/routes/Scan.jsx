import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import Header from 'components/Header';
import ImageUpload from 'components/ImageUpload';
import FingerprintsUpload from 'components/FingerprintsUpload';


export class Scan extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    type: PropTypes.string,
    headerName: PropTypes.string
  };

  render() {
    const { dispatch, headerName, type } = this.props;

    return (
      <div>
        <Header dispatch={dispatch} headerName={headerName} />
        <main className="app__main">
        <div key="Scan" className="app__private app__route">
          <div className="app__container">
              <div className="app__private__header">
                <h1>Now scanning for potential matches</h1>
              </div>
              {type === "fingerprint" && <FingerprintsUpload/>}
              {type === "image" && <ImageUpload/>}
          </div>
        </div>
        </main>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return { user: state.user };
}

export default connect(mapStateToProps)(Scan);

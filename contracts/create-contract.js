var Web3 = require('web3')
var provider = new Web3.providers.HttpProvider("http://localhost:8544");
var contract = require("truffle-contract");
var dataRegistryArtifact = require("./DataRegistry.json");
import {BigNumber} from "bignumber.js"; 

var MyContract = contract(dataRegistryArtifact)
MyContract.setProvider(provider); // => a promise

// Later...
var deployed;
MyContract.deployed().then(function(instance) {
  var deployed = instance;
  return instance.addData();
}).then(function(result) {
  console.log('result', result);
  // Do something with the result or continue with more transactions.
}, err => console.error(err));
